import React from 'react';
import store from './../../../redux/store';
import ACTION from './../../../redux/action';

// Component with Passed Event Handler as Props
/*
const Btn = (props)=>(
    <button onClick={props.onClick}>
        {props.text}
    </button>
);
Btn.propTypes = {
    onClick: React.PropTypes.func,
    text: React.PropTypes.string
}
*/

// Component with Own Event Handler as Props
class Btn extends React.Component {
    componentDidMount() {
        // Ajax Get Data (Alternate: Install & Use axios)
/*        fetch.get('http://dummyurl.com')
        .then((resp)=>{
            // process resp
            store.dispatch({type: ACTION.SOMETHING, param: resp})
        })
        .catch(err=>{
            // error handling
        });  */
    }

    // Event Handler
    onClick() {
        const action = { type: ACTION.ONE, param: null };
        store.dispatch(action);
    }
	render() {
		return (
			<button onClick={this.onClick}>
                {this.props.text}
			</button>
		);
	}
}
Btn.propTypes = {
    text: React.PropTypes.string
}

export default Btn;