//# ES6 & REACT CONVERSION SETUP (via BABEL)
module.exports = {
	test: /\.js$/,
	use: 'babel-loader',
	exclude: /node_modules/
};