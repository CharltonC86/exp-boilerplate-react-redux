var $html = require('html-webpack-plugin');

module.exports = {
	// Webpack plugin - Goes under "webpackConfigObj.plugins"
	plugins: [
		new $html({
			/* Location of the template */
			// - 'src' not './src'
			template: 'src/index.html.ejs',

			/* Minify html */
			minify: {
				collapseWhitespace: true
			},

			/* Define property name & value to be accessed in HTML file, e.g. <%= htmlWebpackPlugin.options.title %> */
			title: 'lorem',

			/* Specify which JS output bundle file should NOT be include in this generated Html file. Used when there are multiple entry js files and you want specific JS bundle file to go with specific generated HTML */
			// excludeChunks: [ 'entryPropName1', 'entryPropName2' ]

			/* Specify (rel. to the 'dist' folder) */
			// filename: require('path').join(__dirname, '..', 'dist/index.html'),

			/* Cache busting (disable caching) by adding random generated string to the "bundle.js" or "style.css" inside <script>or <link>, e.g. src="dist/bundle.js74hfhf7asf" */
			// hash: true
		})
	]
};