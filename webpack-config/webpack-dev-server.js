var webpack = require('webpack');

module.exports = {
	plugins: [
		// Enable Hot reload
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin()
	],
	config: {
		// - writes files in "memory", no physical files
		// - if Webpack v2.x is used, then "webpack-dev-server" also needs to be v2.x
		// which folder to serve all the files
		contentBase: global._path.dist,

		// gzip enabled
		compress: true,

		// Simplified log - Don't show the log
		stats: 'errors-only',

		// Open a new browser window every time webpack-dev-server is run
		open: true,

		// Enable HMR
		hot: true

		// specify port (other than default 8080)
		// port: 9000
	}
};