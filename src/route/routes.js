// Note for <Route>:
// - the attr. `browser={BrowserHistory}` is only applicable to V3
// = the attr. `exact` is applicable to V4
// <Router> can only have a child, therefore requires a <div> to wrap <Route>

import React from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';
// import Btn from './../partial/component/sample/view';

const Demo = ()=>(
    <h1>lorem</h1>
);

const Routes = ()=>(
    <Router>
        <div>
            <Route path="/path" component={Demo} />
        </div>
    </Router>
);

export default Routes;