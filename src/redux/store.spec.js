import ACTION from './action';
import store from './store';

describe('store test', ()=>{

    it('should do update the store based on ACTION.ONE', ()=>{
        const action = {
            type: ACTION.ONE,
            param: null
        };
        store.dispatch(action);
        // expect(store.getState().data).toBe('fail');    // fail
        expect(store.getState().data).toBe('ONE');     // success
    });

    it('should do update the store based on ACTION.TWO', ()=>{
        const action = {
            type: ACTION.TWO,
            param: null
        };
        store.dispatch(action);
        // expect(store.getState().data).toBe('fail');    // fail
        expect(store.getState().data).toBe('TWO');     // success
    });
});