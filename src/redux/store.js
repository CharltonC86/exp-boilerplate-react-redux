import { createStore } from 'redux';
import reducer from './reducer';

const defState = {
    data: 'NONE'
};
const store = createStore(reducer, defState);

export default store;