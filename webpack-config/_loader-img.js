//# IMAGES PROCESSING WITH OPTIONAL IMAGE OPTIMIZATION
// DEP: file-loader, image-webpack-loader
module.exports = {
	test: /\.(png|gif|jpe?g|svg)$/i,
	use: [
		// Compulsory Syntax for `file-loader` (using `image-webpack-loader` query syntax doesnt work)
		// - Alternate syntax: 'file-loader?publicPath=img/&outputPath=img/&name=[name].[ext]',
		// - Images will only be copied to "dist" folder if they are used/referenced, e.g. in html, css
		{
			loader: 'file-loader',
			options: {
				publicPath: 'img/',
				outputPath: 'img/',
				exclude: 'img/sprite',
				name: '[name].[ext]'
			}
		},

		// Compulsory Syntax for `image-webpack-loader` (using `file-loader` syntax doesnt work)
		{
			loader: 'image-webpack-loader',
			options: {
				query: {
					optimizationLevel: 7,
					interlaced: false,
					mozjpeg: { quality: 65 },
					pngquant:{
						quality: "65-90",
						speed: 4
					}
				}
			}
		}
	]
}