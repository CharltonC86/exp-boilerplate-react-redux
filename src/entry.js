// require('./sass/style.scss');
// console.log('lorem sum');

// Library
import React from 'react';
import { render } from 'react-dom';

// App specific
import './sass/style.scss';
import Routes from './route/routes';
// import Btn from './partial/component/sample/view';

// Render to DOM
/*render(
    <Btn text={'btn text'}/>,
    document.getElementById('root')
);*/
render(
    <Routes />,
    document.getElementById('root')
);