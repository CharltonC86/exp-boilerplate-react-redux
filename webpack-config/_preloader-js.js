module.exports = {
    test: /\.js$/,
    include: global._path.src,
    loader: 'eslint-loader',

	//# This is a Preloader
    enforce: 'pre'

    //# No need to specify since "src" is already specified
    // exclude: /node_modules/,
};