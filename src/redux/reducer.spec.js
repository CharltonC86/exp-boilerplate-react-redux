import ACTION from './action';
import reducer from './reducer';

describe('reducer test', ()=>{

    it('should return default state if No action type is specified', ()=>{
        const action = {
            type: null,
            param: null
        };
        const nextState = reducer(null, action);
        // expect(nextState.data).toBe('fail');        // fail
        expect(nextState.data).toBe('NONE');        // success
    });

    it('should return state for ACTION.ONE', ()=>{
        const action = {
            type: ACTION.ONE,
            param: null
        };
        const nextState = reducer(null, action);
        // expect(nextState.data).toBe('fail');        // fail
        expect(nextState.data).toBe('ONE');         // success
    });

    it('should return state for ACTION.TWO', ()=>{
        const action = {
            type: ACTION.TWO,
            param: null
        };
        const nextState = reducer(null, action);
        // expect(nextState.data).toBe('fail');        // fail
        expect(nextState.data).toBe('TWO');         // success
    });
});