var path = require('path');
var $spritesmith = require('webpack-spritesmith');

module.exports = {
    plugins: [
        new $spritesmith({
            // Input
            src: {
                cwd: 'src/img/sprite',
                glob: '*.png'
            },

            // Output Image File & CSS File
            target: {
                image: 'src/img/sprite.png',
                css: 'src/sass/_sprite.scss'
            },

            // Location of Sprite Image the Relative to the result/output css file
            apiOptions: {
                cssImageRef: '../img/sprite.png'
            },

            // Sprite Stitching/Positioning Mechanism
            spritesmithOptions: {
                algorithm: 'top-down'
            }
        })
    ]
};