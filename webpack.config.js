// ## SET FIXED PROJECT FOLDER PATH (__dirname is rel. to webpack.config.js location therefore no need to use `require('path').resolve(__dirname, '..')`) ##
var path = require('path');
global._path = {
	src: __dirname + '/src/',
	dist: __dirname + '/dist/'
}


// ## IMPORT WEBPACK BASE & PLUGIN CONFIG ##
var webpackConfig = require('./webpack-config/_base.js');
var LOADER = {
	style: require('./webpack-config/_loader-style.js'),
	preJs: require('./webpack-config/_preloader-js.js'),
	js: require('./webpack-config/_loader-js.js'),
	img: require('./webpack-config/_loader-img.js')
};
var PLUGIN = {
	htmlWebpack: require('./webpack-config/html-webpack-plugin.js'),
	extractTextWebpack: require('./webpack-config/extract-text-webpack-plugin.js'),
	autoprefixer: require('./webpack-config/autoprefixer.js'),
	spritesmith: require('./webpack-config/webpack-spritesmith.js'),
	webpackDevServer: require('./webpack-config/webpack-dev-server.js')
};


// ## CONFIG ##
// - Webpack Loader Config
webpackConfig.module.rules = [
	LOADER.style,
	LOADER.preJs,
	LOADER.js,
	LOADER.img
];
// - Webpack Plugin Config
// 		-- webpack.uglifyJS js is not required in Webpack 2 when `webpack -p` is run in cli)
// 		-- [].concat is required as it is flattening item which is also an array
webpackConfig.plugins = [].concat(
	PLUGIN.htmlWebpack.plugins,
	PLUGIN.spritesmith.plugins,
	PLUGIN.autoprefixer.plugins,
	PLUGIN.extractTextWebpack.plugins,
	PLUGIN.webpackDevServer.plugins
);
// - Plugin Config (outside of Webpack)
webpackConfig.devServer = PLUGIN.webpackDevServer.config;

// ## EXPORT CONFIG
module.exports = webpackConfig;