var $extractText = require('extract-text-webpack-plugin');

module.exports = {
	plugins: [
		//# Specify Output Css filename (a Physical File relative To "dist" Folder)
		new $extractText('css/style.css')
	]
};