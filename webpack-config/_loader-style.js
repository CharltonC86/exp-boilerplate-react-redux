//# SASS/CSS SETUP
var isProd = process.env.NODE_ENV === 'production',
	$extractText = require('extract-text-webpack-plugin');

var webpack = require('webpack'),
	$autoprefixer = require('autoprefixer');	

module.exports = {
	test: /\.scss$/,
	
	
	// DEV (Inline Style in <head>)
	use: [
		'style-loader',
		'css-loader',				
		{ 
			loader: 'postcss-loader', 
			options: {
				plugins: [
					$autoprefixer()
				]
			} 
		},
		'sass-loader?sourceMap'
	]	

	// PROD
	// use: $extractText.extract({
	// 	fallback: 'style-loader',
	// 	use: [
	// 		'css-loader',
	// 		{ 
	// 			loader: 'postcss-loader', 
	// 			options: {
	// 				plugins: [
	// 					$autoprefixer()
	// 				]
	// 			} 
	// 		},
	// 		'sass-loader'
	// 	]
	// })

	// Alternative way to do (for both Dev & Prod)
	// #in this file
	// use: !isProd ?
	// 	['style-loader', 'css-loader', 'sass-loader'] :
	// 	$extractText.extract({
	// 		fallback: 'style-loader',
	// 		use: [ 'css-loader', 'postcss-loader', 'sass-loader?sourceMap' ]
	// 	})	
	
	// #in "webpack.config.js" file 
	// ...
	// module: ...,
	// plugins: [
	// 	new webpack.LoaderOptionsPlugin({
	// 		options: {
	// 			// postcss: [		// means plugin for the `postcss-loader`
	// 			// 	//# alternative: $autoprefixer(prefixerOptionObject)
	// 			// 	$autoprefixer(),
	// 			// ]
	// 		}
	// 	})
	// ]	

}