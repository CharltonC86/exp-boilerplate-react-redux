var isProd = process.env.NODE_ENV === 'production';

console.log(global._path.src)
var baseConfig = {
		//## BUNDLE INPUT & OUTPUT
		entry: global._path.src + 'entry.js',    // requires abs path
		output: {
			path: global._path.dist,             // requires abs path
			filename: 'bundle.js'
		},

		//## LODAERS - FILE PROCESSING CONFIG
		module: {}
	};

// Enable sourcemap if in DEV mode
if (!isProd) baseConfig.devtool = 'source-map';

module.exports = baseConfig;