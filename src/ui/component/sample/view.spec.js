import React from 'react';
import { shallow } from 'enzyme';
import Button from './view';
import store from './../../../redux/store';

/*describe('Component with Passed Event Handler as Props', ()=>{
	const onClick = jest.fn();
	const button = shallow(<Button text="text" onClick={onClick} />);

	test('component type', () => {
		// expect(button.type()).toBe('fail');		// fail
		expect(button.type()).toBe('button');	// success
	});

	test('passed props', () => {
		// expect(button.text()).toBe('fail');		// fail
		expect(button.text()).toBe('text');		// success
	});

	test('passed event handler', () => {
		button.simulate('click');
		// expect(onClick).not.toBeCalled();	// fail
		expect(onClick).toBeCalled();		// success
	});
});*/

describe('Non-Stateless Component with Own Event Handler', ()=>{
	const button = shallow(<Button text="text" />);
	const spiedDispatch = jest.spyOn(store, 'dispatch');

	test('component type', () => {
		// expect(button.type()).toBe('fail');		// fail
		expect(button.type()).toBe('button');	// success
	});

	test('passed props', () => {
		// expect(button.text()).toBe('fail');		// fail
		expect(button.text()).toBe('text');		// success
	});
	test('own event handler', () => {
		button.simulate('click'); 		// OR button.simulate('click', {target: null });

		// expect(onClick).not.toBeCalled();		// fail
		expect(spiedDispatch).toHaveBeenCalledWith({ type: 'ONE', param: null });		// success
	});

	afterEach(()=>{
  		spiedDispatch.mockClear()
	});
});