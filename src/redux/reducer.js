import ACTION from './action';

function reducer (prevState, action) {
    switch (action.type) {
        case ACTION.ONE:
            // handle action one & return new state
            return {
                data: 'ONE'
            };
        case ACTION.TWO:
            // handle action two & return new state
            return {
                data: 'TWO'
            };
        default:
            return {
                data: 'NONE'
            };
    }
}

export default reducer;